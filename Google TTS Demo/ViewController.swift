//
//  ViewController.swift
//  Google TTS Demo
//
//  Created by Alejandro Cotilla on 5/30/18.
//  Copyright © 2018 Alejandro Cotilla. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var speakButton: UIButton!
    
    @IBOutlet weak var voiceCategoryControl: UISegmentedControl!
    @IBOutlet weak var voiceGenderControl: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func didPressSpeakButton(_ sender: Any) {        
        speakButton.setTitle("Speaking...", for: .normal)
        speakButton.isEnabled = false
        speakButton.alpha = 0.6
        
        var voiceType: VoiceType = .undefined
        let category = voiceCategoryControl.titleForSegment(at: voiceCategoryControl.selectedSegmentIndex)
        let gender = voiceGenderControl.titleForSegment(at: voiceGenderControl.selectedSegmentIndex)
        if category == "WaveNet" && gender == "Female" {
            voiceType = .waveNetFemale
        }
        else if category == "WaveNet" && gender == "Male" {
            voiceType = .waveNetMale
        }
        else if category == "Standard" && gender == "Female" {
            voiceType = .standardFemale
        }
        else if category == "Standard" && gender == "Male" {
            voiceType = .standardMale
        }
            
        SpeechService.shared.speak(text: textView.text, voiceType: voiceType) {
            self.speakButton.setTitle("Speak", for: .normal)
            self.speakButton.isEnabled = true
            self.speakButton.alpha = 1
        }
    }
    @IBAction func didPressAvFoundation(_ sender: Any) {
        TextToSpeech(textToSpeech: textView.text)
    }
    
    private func TextToSpeech(textToSpeech: String){
        // Line 1. Create an instance of AVSpeechSynthesizer.
        let speechSynthesizer = AVSpeechSynthesizer()
        // Line 2. Create an instance of AVSpeechUtterance and pass in a String to be spoken.
        let speechUtterance: AVSpeechUtterance = AVSpeechUtterance(string: textToSpeech)
        //Line 3. Specify the speech utterance rate. 1 = speaking extremely the higher the values the slower speech patterns. The default rate, AVSpeechUtteranceDefaultSpeechRate is 0.5
        speechUtterance.rate = AVSpeechUtteranceMaximumSpeechRate / 2.0
        // Line 4. Specify the voice. It is explicitly set to English here, but it will use the device default if not specified.
        speechUtterance.voice = AVSpeechSynthesisVoice(language: "en")
        // Line 5. Pass in the urrerance to the synthesizer to actually speak.
        speechSynthesizer.speak(speechUtterance)
    }
}

